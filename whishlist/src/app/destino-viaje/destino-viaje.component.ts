import { Component, OnInit, Input , HostBinding, Output, EventEmitter} from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { EstadoApp } from '../app.module';
import { VotoArribaAccion, VotoAbajoAccion } from '../models/destinos-viajes-state.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.sass']
})
export class DestinoViajeComponent implements OnInit {

  // Se declaran las variables globales.

  /** Informacion del destino. */
  @Input() destino: DestinoViaje;

  // No se recomienda utilizar variables con nombre custom.
  // Este nombre custom solo funciona fuera del componente y no dentro del mismo.
  /** Numero de control generado. */
  // tslint:disable-next-line:no-input-rename
  @Input('idx') NumControl: number;

  /** Clase para definir el tipo de columna a establecer.  */
  @HostBinding('attr.class') cssClass = 'col-md-4';

  /** Evento de salida al presionar un destino. */
  @Output() Presionado: EventEmitter<DestinoViaje>;

  // Constructor

  /** Constructor del componente. */
  constructor(private Almacenamiento: Store<EstadoApp>) {
    // Se inicializa el evento.
    this.Presionado = new EventEmitter();
   }

   // Procesos

  ngOnInit(): void {
  }

  /** Proceso para seleccionar el destino como favorito. */
  ir(): boolean {
    /* Dispara el evento de precionado el boton de ir. */
    this.Presionado.emit(this.destino);
    /* Se retorna falso para que no recargue la pagina el boton. */
    return false;
  }

  /** Proceso para dar un voto positivo. */
  ManoArriba(): boolean {
    this.Almacenamiento.dispatch(new VotoArribaAccion(this.destino));
    return false;
  }

  /** Proceso para dar un voto negativo. */
  ManoAbajo(): boolean {
    this.Almacenamiento.dispatch(new VotoAbajoAccion(this.destino));
    return false;
  }

}
