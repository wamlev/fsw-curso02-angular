import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { DestinoViaje } from './../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';


@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.sass']
})
export class FormDestinoViajeComponent implements OnInit {

  /** Evento cuando se agrega un item. */
  @Output() ItemAgregado: EventEmitter<DestinoViaje>;
  /** Grupo de controles para seguimiento.  */
  fg: FormGroup;
  /** Número de caracteres como mínimo para la cadena de texto. */
  MinCaracter = 12;
  /** Lista de recomendaciones. */
  Recomendaciones: string[];
  /**
   * Constructor del objeto.
   * @param fb Objeto responsable para la construccion del form group.
   */
  constructor(fb: FormBuilder) {
    this.ItemAgregado = new EventEmitter();
    this.fg = fb.group({
      Nombre: ['', Validators.required],
      URL: ['', Validators.compose([
        Validators.required,
        this.ValidarURL,
        this.ValidarLongitudURL(this.MinCaracter)
      ])]
    });
    /*
    this.fg.valueChanges.subscribe((Form: FormGroup) => {
      console.log('Cambio el formulario: ' + Form.controls);
    });*/
  }

  ngOnInit(): void {
    const InputNombre = document.getElementById('nombre') as HTMLElement;
    fromEvent(InputNombre, 'input')
      .pipe(
        map((tecla: KeyboardEvent) => (tecla.target as HTMLInputElement).value), // Esto mapea el evento a evaluar.
        filter(texto => texto.length > 3), // Filtro requerido, en este caso tamaño de lo escrito.
        debounceTime(200), // Tiempo de espera.
        distinctUntilChanged(), // Distinción de lo escrito al estar corrigiendo.
        switchMap(() => ajax('/assets/recomendaciones.json')) // Accion de obtencion de datos.
      ).subscribe( RespuestaAjax => {
        this.Recomendaciones = RespuestaAjax.response;
      });
  }

  /**
   * Procesp para emitir el evento del destino a almacenar.
   * @param Nombre Nombre del destino.
   * @param URL URL de la imagen del destino.
   */
  Guardar(Nombre: string, URL: string): boolean {
    const Destino = new DestinoViaje(Nombre, URL);
    this.ItemAgregado.emit(Destino);
    return false;
  }

  /**
   * Proceso para validar almenos algun caracter ingresado en el control.
   * @param control Control que se valida.
   */
  ValidarURL(control: FormControl): {[llave: string]: boolean} {
    const Longitud = control.value.toString().trim().length;
    if (Longitud > 0 && Longitud < 5) { return {ImagenInvalida: true}; }
    else { return null; }
  }

  /**
   * Proceso para validar que el control tenga como minimo el numero de caracteres indicado.
   * @param LongitudMin Longitud minima de caracteres.
   */
  ValidarLongitudURL(LongitudMin: number): ValidatorFn {
    return (control: FormControl): {[llave: string]: boolean } | null => {
      const Longitud = control.value.toString().trim().length;
      if (Longitud > 0 && Longitud < LongitudMin) { return {LongitudInvalida: true}; }
      else { return null; }
    };
  }

}
