import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'Whishlist';
  Fecha = new Observable( Observador => {
    setInterval(() => Observador.next(new Date().toString()), 1000); // Configuracion del intervalo de tiempo.
  });
}
