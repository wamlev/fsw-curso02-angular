// Se importan las clases requeridas.
import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';

/**
 * Objeto para llevar el control de la lista de destinos.
 */
export class DestinosApiClient {

  // Atributos privados.

  /** Listado de los destinos. */
  private ListaDestinos: DestinoViaje[];

  /** Destino actual que se tiene activo. */
  private DestinoActual: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

  // Constructor de la clase.

  constructor(){
    // Se inicializa la lista de destinos.
    this.ListaDestinos = [];
  }

  // Metodos publicos

  /**
   * Proceso para agregar el destino a la lista.
   * @param Destino Destino que se agregara a la lista.
   */
  public add(Destino: DestinoViaje): void{ this.Agregar(Destino); }
  /** Proceso para obtener la lista actual de los servicios. */
  public getAll(): DestinoViaje[]{ return this.ObtenerLista();  }

  /**
   * Proceso para obtener un destino por el identificador.
   * @param id Dato para la busqueda.
   */
  public getById(id: string): DestinoViaje { return this.ObtenerDestino(id); }

  /**
   * Proceso para marcar un destino como marcado favorito.
   * @param Destino Destino seleccionado.
   */
  public setSelected(Destino: DestinoViaje): void { this.Seleccionar(Destino); }

  /**
   * Proceso para realizar la subscripcion observable.
   * @param fn Proceso de ejecucion en la suscripcion.
   */
  public subscribeOnChange(fn: any): void { this.Subscripcion(fn); }

  // Metodos privados

  /**
   * Proceso para agregar el destino a la lista.
   * @param Destino Destino que se agregara a la lista.
   */
  private Agregar(Destino: DestinoViaje): void{
    this.ListaDestinos.push(Destino);
  }

  /** Proceso para obtener la lista actual de los servicios. */
  private ObtenerLista(): DestinoViaje[]{
    return this.ListaDestinos;
  }

  /**
   * Proceso para obtener un destino por el identificador.
   * @param id Dato para la busqueda.
   */
  private ObtenerDestino(id: string): DestinoViaje {
    const Destino = this.ListaDestinos.filter( DestinoBusqueda => DestinoBusqueda.nombre.toString() === id )[0];
    return Destino;
  }

  /**
   * Proceso para marcar un destino como marcado favorito.
   * @param Destino Destino seleccionado.
   */
  private Seleccionar(Destino: DestinoViaje): void {
    this.ListaDestinos.forEach(DestinoAux => DestinoAux.setSelected(false));
    Destino.setSelected(true);
    this.DestinoActual.next(Destino);
  }

  /**
   * Proceso para realizar la subscripcion observable.
   * @param fn Proceso de ejecucion en la suscripcion.
   */
  private Subscripcion(fn: any): void {
    this.DestinoActual.subscribe(fn);
  }

}
