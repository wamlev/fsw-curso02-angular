// Librerias a utilizar.
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
// Objetos a utilizar.
import { DestinoViaje } from './destino-viaje.model';

/** Interface para el estado de los destinos de viajes. */
export interface DestinosViajesEstado {
  /** Lista de destinos. */
  Lista: DestinoViaje[];
  /** Indicador de carga de destinos. */
  Cargando: boolean;
  /** Destino favorito. */
  Favorito: DestinoViaje;
}

/** Inicializacion por defecto del estado de los destinos de viajes.  */
export const InicializarDestinosViajesEstado = (): DestinosViajesEstado => {
  return {
    Lista: [],
    Cargando: false,
    Favorito: null
  };
};

/** Enumeracion de los tipos de acciones de los destinos de viajes. */
export enum DestinosViajesAccionesTipos {
  /** Destino nuevo. */
  NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
  /** Destino favorito. */
  FAVORITO_DESTINO = '[Destinos Viajes] Favorito',
  /** Voto a favor de destino. */
  VOTO_ARRIBA = '[Destinos Viajes] Voto Arriba',
  /** Voto a en contra de destino. */
  VOTO_ABAJO = '[Destinos Viajes] Voto Abajo'
}

/**
 * Definición de la accion de la inserción de un nuevo destino.
 * type: comando reservado al implementar Action.
 */
export class NuevoDestinoAccion implements Action {
  /** Tipo de movimiento. */
  type = DestinosViajesAccionesTipos.NUEVO_DESTINO;
  /**
   * Constructor del objeto.
   * @param Destino Destino que se ingresa como nuevo.
   */
  constructor(public Destino: DestinoViaje) {}
}

/**
 * Definición de la accion de la selección de destino como favorito.
 * type: comando reservado al implementar Action.
 */
export class FavoritoDestinoAccion implements Action {
  /** Tipo de movimiento. */
  type = DestinosViajesAccionesTipos.FAVORITO_DESTINO;
  /**
   * Constructor del objeto.
   * @param Destino Destino que se selecciona como Favorito.
   */
  constructor(public Destino: DestinoViaje) {}
}

/**
 * Definición de la accion de aumento en la votación del destino.
 * type: comando reservado al implementar Action.
 */
export class VotoArribaAccion implements Action {
  /** Tipo de movimiento. */
  type = DestinosViajesAccionesTipos.VOTO_ARRIBA;
  /**
   * Constructor del objeto.
   * @param Destino Destino que se selecciona como Favorito.
   */
  constructor(public Destino: DestinoViaje) {}
}

/**
 * Definición de la accion de decremento en la votación del destino.
 * type: comando reservado al implementar Action.
 */
export class VotoAbajoAccion implements Action {
  /** Tipo de movimiento. */
  type = DestinosViajesAccionesTipos.VOTO_ABAJO;
  /**
   * Constructor del objeto.
   * @param Destino Destino que se selecciona como Favorito.
   */
  constructor(public Destino: DestinoViaje) {}
}

/** Definición de tipo de dato para las posibles acciones de destinos de viajes. */
export type DestinosViajesAcciones = NuevoDestinoAccion | FavoritoDestinoAccion | VotoAbajoAccion | VotoArribaAccion;

/**
 * Proceso reductor para actualizar o agregar los destinos de viajes.
 * @param Estado Estado actual de los destinos de viajes.
 * @param Accion Accion que se ejecuta con el destino de viaje incluido.
 */
export function ReductorDestinosViajes(
  Estado: DestinosViajesEstado,
  Accion: DestinosViajesAcciones
): DestinosViajesEstado {
  switch (Accion.type) {
    case DestinosViajesAccionesTipos.NUEVO_DESTINO: {
      return {
        ...Estado,
        Lista: [...Estado.Lista, (Accion as NuevoDestinoAccion).Destino]
      };
    }
    case DestinosViajesAccionesTipos.FAVORITO_DESTINO: {
      Estado.Lista.forEach(Destino => Destino.setSelected(false));
      const FavoritoSel: DestinoViaje = (Accion as FavoritoDestinoAccion).Destino;
      FavoritoSel.setSelected(true);
      return{
        ...Estado,
        Favorito: FavoritoSel
      };
    }
    case DestinosViajesAccionesTipos.VOTO_ABAJO: {
      const Destino: DestinoViaje = (Accion as VotoAbajoAccion).Destino;
      Destino.VoteDown();
      return{ ...Estado };
    }
    case DestinosViajesAccionesTipos.VOTO_ARRIBA: {
      const Destino: DestinoViaje = (Accion as VotoArribaAccion).Destino;
      Destino.VoteUp();
      return{ ...Estado };
    }
  }
  return Estado;
}

/** Efecto de accion. */
@Injectable()
export class EfectoDestinosViajes {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.Acciones$.pipe(
    ofType(DestinosViajesAccionesTipos.NUEVO_DESTINO),
    map((Accion: NuevoDestinoAccion) => new FavoritoDestinoAccion(Accion.Destino))
  );
  constructor(private Acciones$: Actions) {}
}

