export class DestinoViaje{

  // Atributos publicos

  /** Servicios con los que cuenta el destino. */
  public servicios: string[];

  // Atributos privados

  /** Indicador para confirmar la seleccion del objeto. */
  private seleccionado: boolean;

  // Constructor.

  /**
   * Constructor del objeto.
   * @param nombre Nombre del destino.
   * @param imagenURL URL válida de la imagen del destino.
   * @param Votos Numero en la votación del destino.
   */
  constructor(public nombre: string, public imagenURL: string, public Votos: number = 0 ){
    // Se inicializa los servicios.
    this.servicios = ['Piscina', 'Desayuno', 'Restaurante'];
   }

   // Metodos publicos.

   /** Proceso para obtener el estado de seleccion. */
  public IsSelected(): boolean { return this.Verificar(); }

  /**
   * Proceso para activar el estado de seleccionado.
   * @param estado Estado en que se desea establecer el objeto.
   */
  public setSelected(estado: boolean): void { this.Seleccionar(estado); }

  /** Proceso para aumentar la votación del destino. */
  public VoteUp(): void { this.VotoArriba(); }

  /** Proceso para disminuir la votación del destino. */
  public VoteDown(): void { this.VotoAbajo(); }

  // Metodos privados.

  /**
   * Proceso para activar el estado de seleccionado.
   * @param estado Estado en que se desea establecer el objeto.
   */
  private Seleccionar(estado: boolean): void {
    this.seleccionado = estado;
  }

  /** Proceso para obtener el estado de seleccion. */
  private Verificar(): boolean { return this.seleccionado; }

  /** Proceso para aumentar la votación del destino. */
  private VotoArriba(): void { this.Votos ++; }

  /** Proceso para disminuir la votación del destino. */
  private VotoAbajo(): void { this.Votos --; }

}
