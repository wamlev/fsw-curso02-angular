import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActionReducerMap, StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppComponent } from './app/app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';
import { DestinosApiClient } from './models/destinos-api-client.model';
import { DestinosViajesEstado, ReductorDestinosViajes, InicializarDestinosViajesEstado, EfectoDestinosViajes } from './models/destinos-viajes-state.model';

/**
 * Definicion de las rutas del aplicativo.
 */
const Rutas: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: ListaDestinosComponent},
  {path: 'destino', component: DestinoDetalleComponent}
];

// redux init
// Ojo debe coincidir el "feature" en este caso "Destinos"

/** Interface para los estados de la aplicacion con respecto a los destinos. */
export interface EstadoApp {
  Destinos: DestinosViajesEstado;
}
/** Reductores del Estado App */
const Reductores: ActionReducerMap<EstadoApp> = {
  Destinos: ReductorDestinosViajes
};

/** Reductor inicializador del estado de los destinos de viajes. */
const ReductorInicilizadorEstado = {
  Destinos: InicializarDestinosViajesEstado()
};

// redux fin init

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(Rutas),
    NgRxStoreModule
      .forRoot
      (
        Reductores,
        {
          initialState: ReductorInicilizadorEstado,
          runtimeChecks: {
            strictStateImmutability: false,
            strictActionImmutability: false
          }
        }
      ), // initialState: Nombre reservado.
    EffectsModule.forRoot([EfectoDestinosViajes]), // Al ser un array se puede agregar los efectos deseados.
    StoreDevtoolsModule.instrument()
  ],
  providers: [
    DestinosApiClient
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
