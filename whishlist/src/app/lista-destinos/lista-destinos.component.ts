import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from '../models/destinos-api-client.model';
import { NuevoDestinoAccion, FavoritoDestinoAccion } from '../models/destinos-viajes-state.model';
import { Store, State } from '@ngrx/store';
import { EstadoApp } from '../app.module';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.sass']
})
export class ListaDestinosComponent implements OnInit {

  // Se declaran las variables globales del componente.
  destinos: DestinoViaje[];

  /** Evento cuando se agrega un item. */
  @Output() ItemAgregado: EventEmitter<DestinoViaje>;

  /** Seguimiento de la actividad generada como favoritos. */
  Movimientos: string[];
  /** Lista cargada de forma reactiva. */
  ListaDestinos;

  /**
   * Constructor del objeto.
   * @param destinoApiClient Api de los destinos.
   * @param Almacenamiento Almacenamiento con Redux.
   */
  constructor(public destinoApiClient: DestinosApiClient, private Almacenamiento: Store<EstadoApp>) {
    // Se inicializa el arreglo como vacio.
    /* this.destinos = []; */
    /** Se activa el emisor de eventos */
    this.ItemAgregado = new EventEmitter();
    /** Se inicializa el registro como vacido de los movimientos. */
    this.Movimientos = [];

    this.Almacenamiento.select(Estado => Estado.Destinos.Favorito).subscribe(
      Favorito => {
        if ( Favorito != null ) {
          this.Movimientos.push('"' + Favorito.nombre + '" ha sido seleccionado como favorito.');
        }
      }
    );

    Almacenamiento.select(Estado => Estado.Destinos.Lista).subscribe(Lista => this.ListaDestinos = Lista);

    /*
    this.destinoApiClient.subscribeOnChange(
      (Destino: DestinoViaje) => {
        if (Destino != null) {
          this.Movimientos.push('Se ha seleccionado el siguiente destino como preferido: ' + Destino.nombre);
        }
      }
    );
    */
  }

  ngOnInit(): void {
  }

  /*
  Guardar(Nombre: string, URL: string): boolean {
    // Se agrega el destino nuevo.
    this.destinos.push(new DestinoViaje(Nombre, URL));
    // Se retorna como falso para evitar la recarga de la pagina.
    return false;
  }
  */

  /**
   * Proceso para asignar el destino indicado.
   * @param Destino Destino ha agregar.
   */
  Agregado(Destino: DestinoViaje): void {
    this.destinoApiClient.add(Destino);
    this.Almacenamiento.dispatch(new NuevoDestinoAccion(Destino));
    this.ItemAgregado.emit(Destino);
  }

  /*
  Seleccionado(destino: DestinoViaje): void {
      this.destinos.forEach( Objeto => { Objeto.setSelected(false); });
      destino.setSelected(true);
  }
  */

  /**
   * Proceso para seleccionar el destino indicado.
   * @param Destino Destino seleccionado.
   */
  Seleccionado(Destino: DestinoViaje): void {
    this.destinoApiClient.setSelected(Destino);
    this.Almacenamiento.dispatch(new FavoritoDestinoAccion(Destino));
  }


}
